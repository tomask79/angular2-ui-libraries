import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MdButtonModule } from '@angular2-material/button';
import { MdCardModule } from '@angular2-material/card';
import { MdTabsModule } from '@angular2-material/tabs';
import { MdMenuModule } from '@angular2-material/menu';
import { MdListModule } from '@angular2-material/list';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdButtonModule.forRoot(), 
    MdCardModule.forRoot(),
    MdTabsModule.forRoot(),
    MdMenuModule.forRoot(),
    MdListModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
