# Writing applications in Angular 2 [part 20] #

## UI component libraries for Angular 2 ##

Just shortly, to develop larger application it's good to have already developed component library. Let's see what are the most known UI libraries for Angular 2

* [Angular 2 Material](https://material.angular.io/) Will be part of the demo.
* [Ng2 BootStrap](http://valor-software.com/ng2-bootstrap/#/)
* [Fuel-UI](http://fuelinteractive.github.io/fuel-ui/#/)
* [Onsen UI](http://onsen.io.s3-website-us-east-1.amazonaws.com/)
* [Ng-BootStrap](https://ng-bootstrap.github.io/#/home)
* [PrimeNG](http://www.primefaces.org/primeng/#/)

## Angular 2 Material Demo ##

First off all, to use Angular 2 material, you need to be at **latest
Angular-CLI**, because Angular 2 material needs at least **Angular 2 RC6**.
Hence to be sure, run the following:

```
npm uninstall -g angular-cli
npm cache clean
npm install -g angular-cli@latest
```

As far as installation is concerned, of course I already prepared for you **package.json** with all necessary dependencies, but in order to start using Angular 2 material components in new project, you need to install them and save version into package.json.

```
npm install --save @angular2-material/core @angular2-material/button @angular2-material/card
```
This will install core, button and card components. For more info visit [getting started page](https://github.com/angular/material2/blob/master/GETTING_STARTED.md). After this just import the appropriate component modules and you should be ready to go.

## Test the Angular 2 Material Demo ##

* npm install
* ng build (in the root directory with package.json)
* ng server
* visit http://localhost:4200